package com.radix.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.*;

public class Address {

	@JsonProperty("use")
	private String use;
	@JsonProperty("line")
	private List<String> line = null;
	@JsonProperty("city")
	private String city;
	@JsonProperty("postalCode")
	private String postalCode;

	@JsonProperty("use")
	public String getUse() {
		return use;
	}

	@JsonProperty("use")
	public void setUse(String use) {
		this.use = use;
	}

	@JsonProperty("line")
	public List<String> getLine() {
		return line;
	}

	@JsonProperty("line")
	public void setLine(List<String> line) {
		this.line = line;
	}

	@JsonProperty("city")
	public String getCity() {
		return city;
	}

	@JsonProperty("city")
	public void setCity(String city) {
		this.city = city;
	}

	@JsonProperty("postalCode")
	public String getPostalCode() {
		return postalCode;
	}

	@JsonProperty("postalCode")
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

}
