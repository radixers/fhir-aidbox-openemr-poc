package com.radix.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.*;

public class PatientDTO {

	@JsonProperty("resourceType")
	private String resourceType;
	@JsonProperty("name")
	private List<Name> name = null;
	@JsonProperty("gender")
	private String gender;
	@JsonProperty("birthDate")
	private String birthDate;
	@JsonProperty("telecom")
	private List<Telecom> telecom = null;
	@JsonProperty("address")
	private List<Address> address = null;

	public String fullName;

	public PatientDTO() {
		super();
	}

	@JsonProperty("resourceType")
	public String getResourceType() {
		return resourceType;
	}

	@JsonProperty("resourceType")
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	@JsonProperty("name")
	public List<Name> getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(List<Name> name) {
		this.name = name;
	}

	@JsonProperty("gender")
	public String getGender() {
		return gender;
	}

	@JsonProperty("gender")
	public void setGender(String gender) {
		this.gender = gender;
	}

	@JsonProperty("birthDate")
	public String getBirthDate() {
		return birthDate;
	}

	@JsonProperty("birthDate")
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	@JsonProperty("telecom")
	public List<Telecom> getTelecom() {
		return telecom;
	}

	@JsonProperty("telecom")
	public void setTelecom(List<Telecom> telecom) {
		this.telecom = telecom;
	}

	@JsonProperty("address")
	public List<Address> getAddress() {
		return address;
	}

	@JsonProperty("address")
	public void setAddress(List<Address> address) {
		this.address = address;
	}

	public String getFullName() {
		return name.get(0).getGiven().get(0).trim() + " " + name.get(0).getFamily().trim();

	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

}
