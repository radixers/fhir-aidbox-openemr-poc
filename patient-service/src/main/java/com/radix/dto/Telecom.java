package com.radix.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Telecom {

	@JsonProperty("system")
	private String system;
	@JsonProperty("use")
	private String use;
	@JsonProperty("value")
	private String value;

	@JsonProperty("system")
	public String getSystem() {
		return system;
	}

	@JsonProperty("system")
	public void setSystem(String system) {
		this.system = system;
	}

	@JsonProperty("use")
	public String getUse() {
		return use;
	}

	@JsonProperty("use")
	public void setUse(String use) {
		this.use = use;
	}

	@JsonProperty("value")
	public String getValue() {
		return value;
	}

	@JsonProperty("value")
	public void setValue(String value) {
		this.value = value;
	}

}
