package com.radix.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.radix.entities.Patient;

public interface PatientRepository extends JpaRepository<Patient, Long> {
	
	@Query("select u from Patient u where (u.firstName = :firstName)"
		      +" and (u.lastName = :lastName)")
	  List<Patient> findByFirstNameAndLastName(String firstName,String lastName);

}
