package com.radix.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.dozermapper.core.Mapper;
import com.radix.dto.PatientDTO;
import com.radix.entities.Patient;
import com.radix.repository.PatientRepository;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class PatientController {

	@Autowired
	private Mapper mapper;

	@Autowired
	PatientRepository patientRepository;

	@GetMapping("/patient")
	public ResponseEntity<List<Patient>> getAllPatients(@RequestParam(required = false) String name,
			@RequestParam(required = false) String family) {
		try {
			List<Patient> patients = new ArrayList<Patient>();

			if (name == null && family == null)
				patientRepository.findAll().forEach(patients::add);

			else
				patientRepository.findByFirstNameAndLastName(name, family).forEach(patients::add);

			if (patients.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(patients, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/patient")
	public ResponseEntity<Patient> createTutorial(@RequestBody PatientDTO patientDto) {
		try {
			Patient patient = new Patient();
			mapper.map(patientDto, patient);
			System.out.println("Patient:::" + patient.toString());
			patientRepository.save(patient);
			return new ResponseEntity<>(patient, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
