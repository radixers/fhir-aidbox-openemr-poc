package com.radix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class IntegrationSynchServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntegrationSynchServiceApplication.class, args);
	}

}
