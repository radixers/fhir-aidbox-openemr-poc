package com.radix.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.radix.entities.Patient;
import com.radix.repository.PatientRepository;
import com.radix.service.PatientDashOpenErmService;
import com.radix.service.PatientService;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class DashPatientSyncController {

	@Autowired
	PatientRepository patientRepository;

	@Autowired
	PatientService patientService;

	@Autowired
	PatientDashOpenErmService patientDashOpenErmService;

	@GetMapping("/patient/aidBox")
	public ResponseEntity<Map<String, Object>> synchDashAidbox() {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Patient> patients = new ArrayList<Patient>();
			patientRepository.findAll().forEach(patients::add);
			patientService.getNamesOfDash(patients);
			patientService.getNamesOfAdibox();
			patientService.findMismatchDataOfAidbox();
			patientService.findMismatchDataOfDash();

			if (patients.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			result.put("count", patientRepository.count());
			result.put("resultList", patientRepository.findAll());
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/patient/openERM")
	public ResponseEntity<Map<String, Object>> synchDashOpenErm() {
		Map<String, Object> resultOpenErm = new HashMap<>();
		try {
			List<Patient> patientsOpenErm = new ArrayList<Patient>();
			patientRepository.findAll().forEach(patientsOpenErm::add);
			patientDashOpenErmService.getNamesOfDash(patientsOpenErm);
			patientDashOpenErmService.getNamesOfOpenErm();
			patientDashOpenErmService.findMismatchDataOfOpenErm();
			patientDashOpenErmService.findMismatchDataOfDash();

			if (patientsOpenErm.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			resultOpenErm.put("count", patientRepository.count());
			resultOpenErm.put("resultList", patientRepository.findAll());
			return new ResponseEntity<>(resultOpenErm, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
