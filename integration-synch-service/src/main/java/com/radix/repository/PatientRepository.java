package com.radix.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.radix.entities.Patient;

public interface PatientRepository extends CrudRepository<Patient, Long> {
	
	@Query("select u from Patient u where (u.firstName = :firstName)"
		      +" and (u.lastName = :lastName)")
	  List<Patient> findByFirstNameAndLastName(String firstName,String lastName);

	
	@Query(value="select * from Patient u where u.full_name IN (:full_name)",nativeQuery = true)
	List<Patient> findByFullName(@Param("full_name")List<String> full_name);

	
	@Query(value="select u.ID from Patient u where u.full_name IN (:full_name) order by full_name desc",nativeQuery = true)
	List<Integer> findIdByFullName(@Param("full_name")List<String> full_name);
}
