package com.radix.utillity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.radix.dto.Token;
import com.radix.service.PatientService;

public class CommonUtil {

	private static final Logger log = LoggerFactory.getLogger(CommonUtil.class);

	@Value("${generateToken.url}")
	private String generateTokenurl;

	@Value("${clientId}")
	private String clientId;

	@Value("${grantType}")
	private String grantType;

	@Value("${userRole}")
	private String userRole;

	@Value("${username}")
	private String username;

	@Value("${username}")
	private String password;

	static String uri = "http://10.10.3.41/oauth2/default/token";
	private static RestTemplate restTemplate = new RestTemplate();

	public static HttpEntity getAidBoxHeadersEntity() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Basic cmFkaXhkZXY6OFFVVU10amhDOTlwSzNnXw==");
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		return entity;
	}

	public static HttpHeaders getHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Basic cmFkaXhkZXY6OFFVVU10amhDOTlwSzNnXw==");
		return headers;
	}

	public static String getAUthorizationTokenInHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("client_id", "oKejjWp0dfVJk7EdFXB6hM_9h_gCGRhVZyiqvYrlqoc");
		map.add("grant_type", "password");
		map.add("user_role", "users");
		map.add("scope",
				"openid api:oemr api:fhir api:port api:pofh user/allergy.read user/allergy.write user/appointment.read user/appointment.write user/dental_issue.read user/dental_issue.write user/document.read user/document.write user/drug.read user/encounter.read user/encounter.write user/facility.read user/facility.write user/immunization.read user/insurance.read user/insurance.write user/insurance_company.read user/insurance_company.write user/insurance_type.read user/list.read user/medical_problem.read user/medical_problem.write user/medication.read user/medication.write user/message.write user/patient.read user/patient.write user/practitioner.read user/practitioner.write user/prescription.read user/procedure.read user/soap_note.read user/soap_note.write user/surgery.read user/surgery.write user/vital.read user/vital.write user/AllergyIntolerance.read user/CareTeam.read user/Condition.read user/Encounter.read user/Immunization.read user/Location.read user/Medication.read user/MedicationRequest.read user/Observation.read user/Organization.read user/Organization.write user/Patient.read user/Patient.write user/Practitioner.read user/Practitioner.write user/PractitionerRole.read user/Procedure.read patient/encounter.read patient/patient.read patient/Encounter.read patient/Patient.read");
		map.add("username", "radixdev");
		map.add("password", "8QUUMtjhC99pK3g_");
		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		ResponseEntity<Token> response = restTemplate.postForEntity(uri, request, Token.class);
		// log.info("Response of token:::" + response.getBody());
		return response.getBody().getAccessToken();
	}

	public static HttpHeaders getAuthHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Bearer " + getAUthorizationTokenInHeaders());
		return headers;
	}

}
