package com.radix.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FHIRResponseDTO {

	@JsonProperty("resourceType")
	private String resourceType;
	@JsonProperty("type")
	private String type;
	@JsonProperty("meta")
	private Meta meta;
	@JsonProperty("total")
	private Integer total;
	@JsonProperty("link")
	private List<Link> link = null;
	@JsonProperty("entry")
	private List<Entry> entry = null;
	
	public String fullName;

	@JsonProperty("resourceType")
	public String getResourceType() {
		return resourceType;
	}

	@JsonProperty("resourceType")
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("meta")
	public Meta getMeta() {
		return meta;
	}

	@JsonProperty("meta")
	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	@JsonProperty("total")
	public Integer getTotal() {
		return total;
	}

	@JsonProperty("total")
	public void setTotal(Integer total) {
		this.total = total;
	}

	@JsonProperty("link")
	public List<Link> getLink() {
		return link;
	}

	@JsonProperty("link")
	public void setLink(List<Link> link) {
		this.link = link;
	}

	@JsonProperty("entry")
	public List<Entry> getEntry() {
		return entry;
	}

	@JsonProperty("entry")
	public void setEntry(List<Entry> entry) {
		this.entry = entry;
	}

	@Override
	public String toString() {
		return "FHIRResponseDTO [resourceType=" + resourceType + ", type=" + type + ", meta=" + meta + ", total="
				+ total + ", link=" + link + ", entry=" + entry + "]";
	}
	
	public String getFullName() {
		return entry.get(0).getResource().getName().get(0).getGiven().get(0).trim()+ " " +entry.get(0).getResource().getName().get(0).getFamily().trim();

	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}


}
