package com.radix.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Entry {

	@JsonProperty("resource")
	private Resource resource;
	@JsonProperty("fullUrl")
	private String fullUrl;

	@JsonProperty("resource")
	public Resource getResource() {
		return resource;
	}

	@JsonProperty("resource")
	public void setResource(Resource resource) {
		this.resource = resource;
	}

	@JsonProperty("fullUrl")
	public String getFullUrl() {
		return fullUrl;
	}

	@JsonProperty("fullUrl")
	public void setFullUrl(String fullUrl) {
		this.fullUrl = fullUrl;
	}

	@Override
	public String toString() {
		return "Entry [resource=" + resource + ", fullUrl=" + fullUrl + "]";
	}

}
