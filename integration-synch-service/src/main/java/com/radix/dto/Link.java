package com.radix.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Link {

	@JsonProperty("relation")
	private String relation;
	@JsonProperty("url")
	private String url;

	@JsonProperty("relation")
	public String getRelation() {
		return relation;
	}

	@JsonProperty("relation")
	public void setRelation(String relation) {
		this.relation = relation;
	}

	@JsonProperty("url")
	public String getUrl() {
		return url;
	}

	@JsonProperty("url")
	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "Link [relation=" + relation + ", url=" + url + "]";
	}

}
