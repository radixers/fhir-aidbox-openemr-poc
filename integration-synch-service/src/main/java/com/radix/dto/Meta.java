package com.radix.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Meta {

	@JsonProperty("versionId")
	private String versionId;

	@JsonProperty("versionId")
	public String getVersionId() {
		return versionId;
	}

	@JsonProperty("versionId")
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}

	@Override
	public String toString() {
		return "Meta [versionId=" + versionId + "]";
	}

}
