package com.radix.dto;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Resource {

	@JsonProperty("name")
	private List<Name> name = null;
	@JsonProperty("id")
	private String id;
	@JsonProperty("resourceType")
	private String resourceType;
	@JsonProperty("gender")
	private String gender;
	@JsonProperty("address")
	private List<Address> address = null;
	@JsonProperty("telecom")
	private List<Telecom> telecom = null;
	@JsonProperty("birthDate")
	private String birthDate;

	public String fullName;
	public String mobileNumber;
	public String emailId;

	@JsonProperty("name")
	public List<Name> getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(List<Name> name) {
		this.name = name;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("resourceType")
	public String getResourceType() {
		return resourceType;
	}

	@JsonProperty("resourceType")
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	@JsonProperty("gender")
	public String getGender() {
		return gender;
	}

	@JsonProperty("gender")
	public void setGender(String gender) {
		this.gender = gender;
	}

	@JsonProperty("address")
	public List<Address> getAddress() {
		return address;
	}

	@JsonProperty("address")
	public void setAddress(List<Address> address) {
		this.address = address;
	}

	@JsonProperty("telecom")
	public List<Telecom> getTelecom() {
		return telecom;
	}

	@JsonProperty("telecom")
	public void setTelecom(List<Telecom> telecom) {
		this.telecom = telecom;
	}

	@JsonProperty("birthDate")
	public String getBirthDate() {
		return birthDate;
	}

	@JsonProperty("birthDate")
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	@Override
	public String toString() {
		return "Resource [name=" + name + ", id=" + id + ", resourceType=" + resourceType + ", gender=" + gender
				+ ", address=" + address + ", telecom=" + telecom + ", birthDate=" + birthDate + "]";
	}

	public String getGenderCase() {
		return gender.substring(0).toLowerCase();
	}

	public void setGenderCase(String fullName) {
		this.fullName = fullName;
	}

	public String getFullName() {
		return name.get(0).getGiven().get(0).trim() + " " + name.get(0).getFamily().trim();
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getMobileNumber() {
		return StringUtils.isNotEmpty(telecom.get(0).getValue()) ? telecom.get(0).getValue()
				: telecom.get(2).getValue();
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailId() {
		return StringUtils.isNotEmpty(telecom.get(1).getValue()) ? telecom.get(1).getValue()
				: telecom.get(3).getValue();
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

}
