package com.radix.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Name {

	@JsonProperty("use")
	private String use;
	@JsonProperty("given")
	private List<String> given = null;
	@JsonProperty("family")
	private String family;

	@JsonProperty("use")
	public String getUse() {
		return use;
	}

	@JsonProperty("use")
	public void setUse(String use) {
		this.use = use;
	}

	@JsonProperty("given")
	public List<String> getGiven() {
		return given;
	}

	@JsonProperty("given")
	public void setGiven(List<String> given) {
		this.given = given;
	}

	@JsonProperty("family")
	public String getFamily() {
		return family;
	}

	@JsonProperty("family")
	public void setFamily(String family) {
		this.family = family;
	}

	@Override
	public String toString() {
		return "Name [use=" + use + ", given=" + given + ", family=" + family + "]";
	}

}
