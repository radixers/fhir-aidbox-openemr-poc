package com.radix.Scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.radix.controller.DashPatientSyncController;


@Component
public class Scheduler {
	
	@Autowired
	DashPatientSyncController dashPatientSyncController;
	
	
	//@Scheduled(cron = "0 0/5 * * * ?")
	public void synchOpenErm() 
	{
		dashPatientSyncController.synchDashOpenErm();
	}
	
	//@Scheduled(cron = "0 0/5 * * * ?")
	public void synchAidBox() 
	{
		dashPatientSyncController.synchDashAidbox();
	}

}
