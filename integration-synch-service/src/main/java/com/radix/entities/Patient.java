package com.radix.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "patient")
public class Patient {

	/*
	 * @GeneratedValue(strategy = GenerationType.AUTO) private Integer id;
	 */
	@Column
	private String firstName;

	@Column
	private String lastName;

	@Column(name = "mobileNumber")
	private String mobileNumber;

	@Column(name = "emailId")
	private String emailId;

	@Column(name = "gender")
	private String gender;

	@Column(name = "addressLine")
	private String addressLine;

	@Column(name = "city")
	private String city;

	@Column(name = "postalCode")
	private String postalCode;

	@Column(name = "birthDate")
	private String birthDate;

	@Id
	@Column(name = "fullName")
	private String fullName;
	
	public String useName="official";
	public String resourceType="Patient";
	public String useTelecom="mobile";
	public String useAddress="home";
	public String systemPhoneTelecom="phone";
	public String systemEmailTelecom="email";
	
	

	public Patient() {
		super();
	}

	public Patient(String firstName, String lastName, String mobileNumber, String emailId, String gender,
			String addressLine, String city, String postalCode, String birthDate, String fullName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobileNumber = mobileNumber;
		this.emailId = emailId;
		this.gender = gender;
		this.addressLine = addressLine;
		this.city = city;
		this.postalCode = postalCode;
		this.birthDate = birthDate;
		this.fullName = fullName;
	}



	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddressLine() {
		return addressLine;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@Override
	public String toString() {
		return "Patient [firstName=" + firstName + ", lastName=" + lastName + ", mobileNumber="
				+ mobileNumber + ", emailId=" + emailId + ", gender=" + gender + ", addressLine=" + addressLine
				+ ", city=" + city + ", postalCode=" + postalCode + ", birthDate=" + birthDate + ", fullName="
				+ fullName + "]";
	}

	public String getUseName() {
		return useName;
	}

	public void setUseName(String useName) {
		this.useName = useName;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getUseTelecom() {
		return useTelecom;
	}

	public void setUseTelecom(String useTelecom) {
		this.useTelecom = useTelecom;
	}

	public String getUseAddress() {
		return useAddress;
	}

	public void setUseAddress(String useAddress) {
		this.useAddress = useAddress;
	}

	public String getSystemPhoneTelecom() {
		return systemPhoneTelecom;
	}

	public void setSystemPhoneTelecom(String systemPhoneTelecom) {
		this.systemPhoneTelecom = systemPhoneTelecom;
	}

	public String getSystemEmailTelecom() {
		return "email";
	}

	public void setSystemEmailTelecom(String systemEmailTelecom) {
		this.systemEmailTelecom = systemEmailTelecom;
	}
	
	

}
