package com.radix.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.github.dozermapper.core.Mapper;
import com.radix.dto.Entry;
import com.radix.dto.FHIRResponseDTO;
import com.radix.dto.PatientDTO;
import com.radix.entities.Patient;
import com.radix.repository.PatientRepository;
import com.radix.utillity.CommonUtil;

@Service
public class PatientService {

	private static final Logger log = LoggerFactory.getLogger(PatientService.class);

	@Value("${aidBoxuri}")
	private String aidBoxuri;

	@Autowired
	PatientRepository patientRepository;

	@Autowired
	private Mapper mapper;

	String aidBoxfirstName = "";
	String aidBoxlastName = "";
	String aidBoxfullName = "";
	private RestTemplate restTemplate = new RestTemplate();
	private List<String> namesListDash = new ArrayList<>();
	private List<String> namesListAidbox = new ArrayList<>();
	private List<Patient> aidBoxResponseList = new ArrayList<>();

	public void getNamesOfDash(List<Patient> patients) {
		patients.stream().forEach(patientName -> {
			String fullName = patientName.getFirstName().trim() + " " + patientName.getLastName().trim();
			namesListDash.add(fullName);
		});
		log.info("Names of Patients in Dash::"
				+ namesListDash.stream().distinct().sorted(Comparator.reverseOrder()).collect(Collectors.toList()));
	}

	public void getNamesOfAdibox() {
		try {
			HttpEntity<String> entity = new HttpEntity<String>(CommonUtil.getHeaders());
			ResponseEntity<FHIRResponseDTO> responseEntity = restTemplate.exchange(aidBoxuri, HttpMethod.GET, entity,
					FHIRResponseDTO.class);
			if (responseEntity.getBody().getTotal() > 0) {
				for (int i = 0; i < responseEntity.getBody().getTotal(); i++) {
					aidBoxfirstName = responseEntity.getBody().getEntry().get(i).getResource().getName().get(0)
							.getGiven().get(0).trim();
					aidBoxlastName = responseEntity.getBody().getEntry().get(i).getResource().getName().get(0)
							.getFamily().trim();
					aidBoxfullName = aidBoxfirstName.trim() + " " + aidBoxlastName.trim();
					namesListAidbox.add(aidBoxfullName);
				}
				settingDataFOrUpdatePurpose(responseEntity);
			}
			log.info("Names of Patients in AidBox::::" + namesListAidbox.stream().distinct()
					.sorted(Comparator.reverseOrder()).collect(Collectors.toList()));
			log.info("Total Records in Aidbox::" + responseEntity.getBody().getTotal());
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}

	private void settingDataFOrUpdatePurpose(ResponseEntity<FHIRResponseDTO> responseEntity) {
		try {
			if (responseEntity.getBody().getTotal() > 0) {
				for (Entry entry : responseEntity.getBody().getEntry()) {
					// setting data in FHIRResponseDTO for patient update
					Patient patientfhirResponseDTO = new Patient();
					mapper.map(entry, patientfhirResponseDTO);
					aidBoxResponseList.add(patientfhirResponseDTO);
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	public void findMismatchDataOfAidbox() {
		try {
			List<String> avaiableInAidBoxList = new ArrayList<>();
			List<String> notAvaiableInAidBoxList = new ArrayList<>();
			// for (String fullName : namesListDash) {
			namesListDash.stream().forEach(fullName -> {
				if (namesListAidbox.contains(fullName))
					avaiableInAidBoxList.add(fullName);
				else
					notAvaiableInAidBoxList.add(fullName);
			});
			log.info("Available In AidBox List:"
					+ avaiableInAidBoxList.stream().distinct().sorted(Comparator.reverseOrder()).collect(Collectors.toList()));
			log.info("Not Available In AidBox List:" + notAvaiableInAidBoxList.stream().distinct()
					.sorted(Comparator.reverseOrder()).collect(Collectors.toList()));
			List<Patient> patientData = patientRepository.findByFullName(notAvaiableInAidBoxList);
			log.info("List Of Patients need to push in AidBox::" + patientData);
			pushDataInAidBox(patientData);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

	}

	private void pushDataInAidBox(List<Patient> patientData) {
		try {
			// for (Patient patient : patientData) {
			patientData.stream().forEach(patient -> {
				PatientDTO patientDTO = new PatientDTO();
				mapper.map(patient, patientDTO);
				HttpEntity<PatientDTO> request1 = new HttpEntity<>(patientDTO, CommonUtil.getHeaders());
				ResponseEntity<String> result = restTemplate.postForEntity(aidBoxuri, request1, String.class);
				log.info("Response ::" + result);
			});
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

	}

	public void findMismatchDataOfDash() {
		List<String> avaiableInDashList = new ArrayList<>();
		List<String> notAvaiableInDashList = new ArrayList<>();
		try {
			namesListAidbox.stream().forEach(fullName -> {
				if (namesListDash.contains(fullName)) {
					avaiableInDashList.add(fullName);
				} else {
					notAvaiableInDashList.add(fullName);
				}
			});
			notAvaiableInDashList.stream().forEach(i -> log.info("Not Available In Dash List:" + notAvaiableInDashList
					.stream().distinct().sorted(Comparator.reverseOrder()).collect(Collectors.toList())));
			log.info("List Of Patients need to push in Dash DB::" + notAvaiableInDashList.stream().distinct()
					.sorted(Comparator.reverseOrder()).collect(Collectors.toList()));
			getNamesDataFromAidBox(notAvaiableInDashList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getNamesDataFromAidBox(List<String> notAvaiableInDashList) {
		try {
			log.info("notAvaiableInDashList:::" + notAvaiableInDashList.size());
			List<Patient> patientAidbox = new ArrayList<>();
			// for (String name : notAvaiableInDashList) {
			notAvaiableInDashList.stream().forEach(name -> {
				String newuri = "";
				String data[] = name.split(" ");
				String firstName = data[0];
				String lastName = data[1];
				newuri = aidBoxuri + "?name=" + firstName + "&family=" + lastName;
				HttpEntity<String> entity = new HttpEntity<String>(CommonUtil.getHeaders());
				ResponseEntity<FHIRResponseDTO> responseEntityFromAidboxBasedonNames = restTemplate.exchange(newuri,
						HttpMethod.GET, entity, FHIRResponseDTO.class);
				Patient aidBoxPatient = new Patient();
				mapper.map(responseEntityFromAidboxBasedonNames.getBody(), aidBoxPatient);
				patientAidbox.add(aidBoxPatient);
			});
			pushDataInDashDB(patientAidbox);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	public void pushDataInDashDB(List<Patient> patientAidbox) {
		try {
			log.info("List Of data adding in dash::" + patientAidbox);
			patientRepository.saveAll(patientAidbox);
			updateDataInDash(aidBoxResponseList);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	private void updateDataInDash(List<Patient> aidBoxResponseList) {
		try {
			log.info(":::Data updating in dash::::");
			patientRepository.saveAll(aidBoxResponseList);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

}
