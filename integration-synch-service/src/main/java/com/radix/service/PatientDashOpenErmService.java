package com.radix.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.github.dozermapper.core.Mapper;
import com.radix.dto.Entry;
import com.radix.dto.FHIRResponseDTO;
import com.radix.dto.PatientDTO;
import com.radix.entities.Patient;
import com.radix.repository.PatientRepository;
import com.radix.utillity.CommonUtil;

@Service
public class PatientDashOpenErmService {
	private static final Logger log = LoggerFactory.getLogger(PatientService.class);

	@Value("${openEmruri}")
	private String openEmruri;

	@Autowired
	PatientRepository patientRepository;

	@Autowired
	private Mapper mapper;

	String openErmFirstName = "";
	String openErmLastName = "";
	String openErmFullName = "";
	private RestTemplate restTemplate = new RestTemplate();
	private List<String> namesListDash = new ArrayList<>();
	private List<String> namesListOpenEmr = new ArrayList<>();
	private List<Patient> openEmrResponseList = new ArrayList<>();

	public void getNamesOfDash(List<Patient> patients) {
		patients.stream().forEach(patientName -> {
			String fullName = patientName.getFirstName().trim() + " " + patientName.getLastName().trim();
			namesListDash.add(fullName);
		});
		log.info("Names of Patients in Dash::"
				+ namesListDash.stream().distinct().sorted(Comparator.reverseOrder()).collect(Collectors.toList()));
	}

	public void getNamesOfOpenErm() {
		try {
			HttpEntity<String> entity = new HttpEntity<String>(CommonUtil.getAuthHeaders());
			ResponseEntity<FHIRResponseDTO> responseEntity = restTemplate.exchange(openEmruri, HttpMethod.GET, entity,
					FHIRResponseDTO.class);
			if (responseEntity.getBody().getTotal() > 0) {
				for (int i = 0; i < responseEntity.getBody().getTotal(); i++) {
					openErmFirstName = responseEntity.getBody().getEntry().get(i).getResource().getName().get(0)
							.getGiven().get(0).trim();
					openErmLastName = responseEntity.getBody().getEntry().get(i).getResource().getName().get(0)
							.getFamily().trim();
					openErmFullName = openErmFirstName.trim() + " " + openErmLastName.trim();
					namesListOpenEmr.add(openErmFullName);
				}
				settingDataFOrUpdatePurpose(responseEntity);
			}
			log.info("Names of Patients in OpenBox::::" + namesListOpenEmr.stream().distinct()
					.sorted(Comparator.reverseOrder()).collect(Collectors.toList()));
			log.info("Total Reords in OpenEmr::" + responseEntity.getBody().getTotal());
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}

	private void settingDataFOrUpdatePurpose(ResponseEntity<FHIRResponseDTO> responseEntity) {
		try {
			if (responseEntity.getBody().getTotal() > 0) {
				for (Entry entry : responseEntity.getBody().getEntry()) {
					// setting data in FHIRResponseDTO for patient update
					Patient patientfhirResponseDTO = new Patient();
					mapper.map(entry, patientfhirResponseDTO);
					openEmrResponseList.add(patientfhirResponseDTO);
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	public void findMismatchDataOfOpenErm() {
		List<String> avaiableInOpenEmrList = new ArrayList<>();
		List<String> notAvaiableInOpenEmrList = new ArrayList<>();
		try {
			namesListDash.stream().forEach(fullName -> {
				if (namesListOpenEmr.contains(fullName)) {
					avaiableInOpenEmrList.add(fullName);
				} else
					notAvaiableInOpenEmrList.add(fullName);
			});
			log.info("Available In OpenEmr List:" + avaiableInOpenEmrList.stream().distinct()
					.sorted(Comparator.reverseOrder()).collect(Collectors.toList()));
			log.info("Not Available In OpenEmr List:" + notAvaiableInOpenEmrList.stream().distinct()
					.sorted(Comparator.reverseOrder()).collect(Collectors.toList()));
			List<Patient> patientData = patientRepository.findByFullName((notAvaiableInOpenEmrList));
			log.info("List Of Patients need to push in OpenEmr::" + patientData);
			pushDataInOpenEmr(patientData);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void pushDataInOpenEmr(List<Patient> patientData) {
		try {
			patientData.stream().forEach(patient -> {
				PatientDTO patientDTO = new PatientDTO();
				mapper.map(patient, patientDTO);
				HttpEntity<PatientDTO> request1 = new HttpEntity<>(patientDTO, CommonUtil.getAuthHeaders());
				ResponseEntity<String> result = restTemplate.postForEntity(openEmruri, request1, String.class);
				log.info("Response ::" + result);
			});
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	public void findMismatchDataOfDash() {
		try {
			List<String> avaiableInDashList = new ArrayList<>();
			List<String> notAvaiableInDashList = new ArrayList<>();
			namesListOpenEmr.stream().forEach(fullName -> {
				if (namesListDash.contains(fullName)) {
					avaiableInDashList.add(fullName);
				} else {
					notAvaiableInDashList.add(fullName);
				}
			});
			notAvaiableInDashList.stream().forEach(i -> log.info("Not Available In Dash List:" + notAvaiableInDashList
					.stream().distinct().sorted(Comparator.reverseOrder()).collect(Collectors.toList())));
			log.info("List Of Patients need to push in Dash DB::" + notAvaiableInDashList.stream().distinct()
					.sorted(Comparator.reverseOrder()).collect(Collectors.toList()));
			getNamesDataFromOpenErm(notAvaiableInDashList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void getNamesDataFromOpenErm(List<String> notAvaiableInDashList) {
		try {
			log.info("notAvaiableInDashList:::" + notAvaiableInDashList.size());
			List<Patient> patientOpenErm = new ArrayList<>();
			notAvaiableInDashList.stream().forEach(name -> {
				String newuri = "";
				String data[] = name.split(" ");
				String firstName = data[0];
				String lastName = data[1];
				newuri = openEmruri + "?name=" + firstName + "&family=" + lastName;
				HttpEntity<String> entity = new HttpEntity<String>(CommonUtil.getAuthHeaders());
				ResponseEntity<FHIRResponseDTO> responseEntityFromAidboxBasedonNames = restTemplate.exchange(newuri,
						HttpMethod.GET, entity, FHIRResponseDTO.class);
				Patient openErmPatient = new Patient();
				mapper.map(responseEntityFromAidboxBasedonNames.getBody(), openErmPatient);
				patientOpenErm.add(openErmPatient);
			});
			pushDataInDashDB(patientOpenErm);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	public void pushDataInDashDB(List<Patient> patientOpenErm) {
		try {
			log.info("List Of data putting in dash::" + patientOpenErm);
			patientRepository.saveAll(patientOpenErm);
			updateDataInDash(openEmrResponseList);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	private void updateDataInDash(List<Patient> openEmrResponseList) {
		try {
			log.info("List Of data updating in dash::");
			patientRepository.saveAll(openEmrResponseList);
		} catch (Exception e) {
			log.info(e.getMessage());
		}
	}

}
